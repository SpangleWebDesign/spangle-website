$(document).ready(function(){
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        if (scroll > 60) {
            $(".navbar").removeClass("navbarStandard");
          $(".navbar").addClass("navbarShrink");
          $(".logoHolder").addClass("logoOverride");
          $(".company").addClass("companyOverride");
          $(".navContact").addClass("navContactOver");
        }
        else{
            $(".navbar").removeClass("navbarShrink");
            $(".navbar").addClass("navbarStandard");
            $(".logoHolder").removeClass("logoOverride");
            $(".company").removeClass("companyOverride");
            $(".navContact").removeClass("navContactOver");
        }
    })
  })